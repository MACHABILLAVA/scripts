/******************************************************************************
 * File Name    - secure_rom_boot.h
 * 
 * Description  - This is the header file for the secure rom boot source code.
 ******************************************************************************/

#ifndef SOURCE_SECURE_ROM_BOOT_H_
#define SOURCE_SECURE_ROM_BOOT_H_



/*******************************************************************************
* Global constants
*******************************************************************************/
#define FLASH_END_ADDR_MEM          0x1000_0000
#define FLASH_MAGIC_NUMBER_ADDR     0x1000_0004
#define FLASH_BOOT_IS_VALID_ADDR    0x1000_0006
#define FLASH_BOOT_VERSION_ADDR     0x1000_0008

#define FLASH_MAGIC_NUMBER          0x5A5A
#define FLASH_BOOT_VALID            0xF1
#define FLASH_BOOT_NOT_VALID        0xF0

/*******************************************************************************
* Structures and enumerations
*******************************************************************************/
typedef enum {
    BOOT_SUCCESS, 
    BOOT_VALIDATION_FAIL,
    BOOT_CRYPT_ERROR,
    BOOT_FLASH_VERSION_ERROR, 
    BOOT_FLASH_BOOT_CORRUPT
} boot_result_t;

/*******************************************************************************
* Function prototypes
*******************************************************************************/



#endif /* SOURCE_SECURE_ROM_BOOT_H_ */

/* [] END OF FILE */